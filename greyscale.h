#ifndef GREYSCALE_H_INCLUDED
#define GREYSCALE_H_INCLUDED

#include <iostream>

class GreyScale
{
    private:
        int breite , hoehe;
        float* mat;
        static void greyError(const char str[]);
    public:
        ~GreyScale(){delete[] mat;};
        GreyScale(int breite = 0,int hoehe = 0);
        GreyScale(const GreyScale&);
        float& operator()(int,int);
        float operator()(int,int) const;
        GreyScale& resize(int,int);
        GreyScale& operator=(const GreyScale&);
        GreyScale& operator+=(const GreyScale&);
        GreyScale& operator-=(const GreyScale&);

        int getWidth() const {
        return breite;
        }
        int getHeight() const {
        return hoehe;
        }

        GreyScale binarize(float) const;
        GreyScale blur() const;
        GreyScale clamp() const;
        GreyScale contrast() const;
        GreyScale kirsch() const;
        GreyScale linTrans(float a,float b) const;
        GreyScale laplace() const;
        GreyScale invert() const;
        GreyScale median() const;
        GreyScale sobel() const;
        GreyScale convolve(const float mask[], int size=3) const;
        void check(std::istream &s);

        friend std::istream& operator>>(std::istream&, GreyScale&);
        friend std::ostream& operator<<(std::ostream&, const GreyScale&);

};

#endif // GREYSCALE_H_INCLUDED
