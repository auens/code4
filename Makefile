CXX = g++
CXXFLAGS = -std=c++17 -Wall -Wextra -Wpedantic -O2 -g

.PHONY: clean

A4: greyscale.o mapraview.o unit.o
	$(CXX) $(CXXFLAGS)  -o $@ $^

mapraview.o: mapraview.cpp greyscale.h unit.h
	$(CXX) $(CXXFLAGS) -c $<
	
greyscale.o: greyscale.cpp unit.h greyscale.h
	$(CXX) $(CXXFLAGS) -c $<

clean:
	rm -f A4 mapraview.o greyscale.o

