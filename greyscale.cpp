#include <cmath>
#include <bits/stdc++.h>
#include "greyscale.h"
#include "unit.h"
//�BERALL SIZE T ZU INT gemacht !
//POTENTIELLER FEHLER: H�HE BREITE GETAUSCHT gemacht!

GreyScale::GreyScale(int b,int h)
{
    if (h < 0) greyError("Nur Vektoren mit positiver hoehe!");
    if (b < 0) greyError("Nur Vektoren mit positiver breite!");
    hoehe = h;
    breite = b;
    mat = new float[h*b];
    for(int i =0;i< h*b;i++) mat[i] = 1;
}




GreyScale::GreyScale(const GreyScale& x){
    hoehe = x.hoehe;
    breite = x.breite;
    mat = new float[hoehe*breite];
    for(int i =0; i < hoehe*breite;i++){
        mat[i]= x.mat[i];
    }
}

float &GreyScale::operator()(int i,int j){
    if((i>breite) or (j > hoehe) or (i < 0) or (j < 0)) greyError("xid1");
    int spot = j*breite+i;
    return mat[spot];
}

float GreyScale::operator()(int i,int j) const{
    if((i>breite+1) or (j > hoehe+1) or (i < -1) or (j < -1)) greyError("lesezugriff drau�en");
    if(i == -1) i = 0;
    if(j == -1) j = 0;
    if(i == breite) i = breite - 1;
    if(j == hoehe) j = hoehe -1;

    int spot = j*breite+i;
    return mat[spot];
}

GreyScale &GreyScale::resize(int b,int h){
    if ((h<0) or (b<0)) greyError("xid");

    delete[] mat;
    hoehe = h;
    breite = b;

    mat = new float[h*b];

    for (int i = 0; i < h*b; i++) mat[i] = 1;
    return *this;
}

GreyScale &GreyScale::operator=(const GreyScale& x){
    if ((hoehe != x.hoehe) or (breite != x.breite))
        (*this).resize(x.breite,x.hoehe);
    for (int i = 0; i < x.hoehe*x.breite; i++) mat[i] = x.mat[i];
    return *this;
}

GreyScale &GreyScale::operator+=(const GreyScale& x){
    if ((hoehe != x.hoehe) or (breite != x.breite)) greyError("xid2");

    for (int i = 0; i < x.hoehe*x.breite; i++)
    {
            mat[i] += x.mat[i];
    }
    return *this;
}

GreyScale &GreyScale::operator-=(const GreyScale& x){
    if ((hoehe != x.hoehe) or (breite != x.breite)) greyError("xid3");

    for (int i = 0; i < x.hoehe*x.breite; i++)
    {
        mat[i] -= x.mat[i];
    }

    return *this;
}

GreyScale GreyScale::binarize(float c) const{
    GreyScale Bin(*this);
    for (int i = 0; i < hoehe*breite; i++)
    {
        if (mat[i] < c)
            Bin.mat[i] = 0;
        else
            Bin.mat[i] = 1;
    }

    return Bin;
}

GreyScale GreyScale::blur() const
{
    float Kt[9]= {0,0.2,0,0.2,0.2,0.2,0,0.2,0};
    return convolve(Kt,3);
}

GreyScale GreyScale::clamp() const
{
    GreyScale Clap(*this);
    for(int i = 0; i < hoehe; i++){
        for(int j = 0; j< breite ; j++){
            if(Clap(j,i)> 1.0) Clap(j,i) = 1.0;
            if(Clap(j,i)< 0.0) Clap(j,i) = 0.0;
        }
    }

    return Clap;
}

GreyScale GreyScale::contrast() const{
    float maxi = 0;
    float mini = 1;
    float eps = 1e-8;
    for(int i = 0; i< hoehe*breite;i++){
        if(maxi < mat[i]) maxi = mat[i];
        if(mini > mat[i]) mini = mat[i];
    }
    if((maxi-mini)< eps){
        maxi = 1;
        mini = 0;
    }

    return linTrans(1.0/(maxi-mini),-mini/(maxi-mini));
}




GreyScale GreyScale::kirsch() const
{
    float Kk[9] = {1,3,3,-1,0,1,-3,-3,-1};
    return convolve(Kk,3);
}

GreyScale GreyScale::laplace() const{
    float Kl[9] = {0,-1,0,-1,4,-1,0,-1,0};
    return convolve(Kl,3);
}

GreyScale GreyScale::linTrans(float a,float b) const
{
    GreyScale trans(*this);
    for (int i = 0; i < hoehe*breite; i++)
        trans.mat[i] = mat[i]*a + b;

    return trans;
}

GreyScale GreyScale::invert() const
{
    return linTrans(-1.0,1.0);
}

GreyScale GreyScale::median() const
{
    float arr[9];
    GreyScale fake(*this);
    for (int x = 0; x < breite; x++)
        for (int y = 0; y < hoehe; y++)
        {
            int counter = 0;
            for (int i = -1; i <= 1; i++)
                for (int j = -1; j <= 1; j++){
                    arr[counter] = (*this)(x+i,y+j);
                    counter++;
                }
            std::sort(arr , arr + 9);
            fake(x,y)= arr[4];
        }
    return fake;
}

GreyScale GreyScale::sobel() const
{
    float DX[9] ={-1,0,1,-2,0,2,-1,0,1};
    float DY[9] ={1,2,1,0,0,0,-1,-2,-1};
    GreyScale fake(*this);
    for (int x = 0; x < breite; x++)
        for (int y = 0; y < hoehe; y++)
        {
            float sum1 = 0;
            float sum2 = 0;
            for (int i = -1; i <= 1; i++)
                for (int j = -1; j <= 1; j++)
                {
                    sum1 += (*this)(x+j,y+i) * DX[(i+1)*3+j+1];
                    sum2 += (*this)(x+j,y+i) * DY[(i+1)*3+j+1];
                }
            fake(x,y) = sqrt(sum1*sum1+sum2*sum2);
        }
    return fake;
}

GreyScale GreyScale::convolve(const float mask[], int size) const
{
    GreyScale Convid(*this);
    for (int x = 0; x < breite; x++)
        for (int y = 0; y < hoehe; y++)
        {
            float sum = 0;
//            for (int i = -size/2; i <= size/2; i++)
//                for (int j = -size/2; j <= size/2; j++)
//                {
//                    sum += (*this)(x+j,y+i) * mask[(i+1)*3+j+1];
//                }
            for(int i=0; i< size;i++){
                for(int j = 0; j<size;j++){
                    sum += (*this)(x+j-(size/2),y+i-(size/2)) * mask[j+i*size];
                }
            }
            Convid(x,y) = sum;
        }


    return Convid;
}


std::istream &operator>>(std::istream &s, GreyScale &x) {
    int inpo;
    std::string mnr;
    float skra;
    //std::string mull;
    int tmpbreite = 0;
    int tmphoehe =0;
    s >> mnr;
    if(mnr != "P2") GreyScale::greyError("NOT PC");
    x.check(s);
    s >> tmpbreite;
    x.check(s);
    s >> tmphoehe;
    x.resize(tmpbreite,tmphoehe);
    x.check(s);
    s >> skra;

    for(int i=0; i< x.hoehe*x.breite;i++){
        x.check(s);
        s >> inpo;
        x.mat[i] = float(inpo)/skra;
    }
    x.check(s);
    if(s.rdstate()!= std::ios::eofbit) GreyScale::greyError("nicht am ende");

    return s;
}


void GreyScale::check(std::istream &s){
    s >> std::ws;
    while(!s.eof() && s.peek() == '#')
        while(s.peek()!= '\n') s.get();
    s >> std::ws;
}



std::ostream &operator<<(std::ostream &s, const GreyScale &x) {
    s << "P2" << "\n" << x.breite << " " << x.hoehe << "\n" << 255 << "\n ";
    GreyScale fake = x.clamp();
    for(int j = 0; j < fake.hoehe;j++){
        for(int i = 0; i< fake.breite;i++){
             s << std::rint(fake(i,j)*255) << " ";
        } //MODULO
    }
    return s;
}


void GreyScale::greyError(const char str[]) {
    std::cerr << "\ngreyrfehler: " << str << '\n' << std::endl;
    std::abort();
}

